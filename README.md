# hello-dapr

This repository acts as a playground for experimenting and gain experience with [Dapr] capabilities.
We are using `docker` with `docker-compose` only since Kubernetes is in most cases is too much.

## Repository Structure

This repository uses a two layered approach:

1. Small services that demonstrate dapr features in isolation
1. One bigger application that uses these services together to form a (hopefully) more useful application

### Services

Each service provides its own README with details on which parts of Dapr it investigates

| Service | Headline |
| - | - |
| [BattleAPI] | Provides an API to publish events. |
| [BattleLog] | Susbcribes and tracks |

Services by Dapr components

| Component | Services |
| - | - |
| State Management | [BattleAPI] |
| Secret Management | [BattleAPI], [BattleLog] |
| Pub/Sub | [BattleAPI], [BattleLog] |
| Python | [BattleLog] |
| C# | [BattleAPI], |

[BattleAPI]: /BattleApi/
[BattleLog]: /BattleLog/


### Why Dapr

Dapr provides a unified API for a multitude of everyday problems without forcing users to juggle x different SDKs.
It handles service discovery and message routing for us so we can focus on what matters.

![](./img/pubsub-overview-publish-API.jpeg)\
Source: <https://docs.dapr.io/developing-applications/building-blocks/pubsub/pubsub-overview>

Aside of every adressable service, a dapr sidecar process will be spun up that knows the services API.
This provides us with an interfaces into our infrastructure that lets us issue requests via standardized HTTP or gRPC.

![](./img/dapr-for-dotnet-developers_pub-sub.png)\
Source: <https://learn.microsoft.com/en-us/dotnet/architecture/dapr-for-net-developers/publish-subscribe>

If you look closely at the code, this allows us to

1. Message without having to know about any message broker
1. Subscribe to messages without special knowledge about used protocols or SDKs
   * We don't need a special SDK for Lua or C

Service configuration is provided in the [/components](./components) folder. It is possible
and prefered to [subscribe via code](https://github.com/dapr/samples/blob/master/pub-sub-routing/app.js#L20) by
implementing a `GET /dapr/subscribe` or via [_`subscription.yml`_](./components/subscription.yml) component configuration.

#### Resources

* <https://learn.microsoft.com/en-us/dotnet/architecture/dapr-for-net-developers/publish-subscribe>
* <https://docs.dapr.io/developing-applications/building-blocks/pubsub/>
* <https://github.com/dapr/samples/tree/master/hello-docker-compose>
* <https://github.com/Inx51/Lab-Dapr.pubsub>

[Dapr] abstracts away messaging internals

[Dapr]: https://dapr.io/

## Requirements



* Docker Compose version v2.10.2+

## Usage

```bash
docker compose up
```

![demo gif of a working app that starts the docker compose inftrastructure and sends 2 messages via swagger](./img/hello-dapr_demo-gbb2021-r3.gif)

## Decision

This pros outweigh the cons and will adapt our architecture to use Dapr.

### Pros

1. This prototype proves that -- at least in the case of pub/sub -- Dapr is working!
1. It provides us with the possibility to implement pub/sub via standardized HTTP/gRPC
   * We are language and framework agnostic (which is important in a microservice architecture).
   * This enables us to use advanced features in lower level langauges like `C99`, `Lua` or even `bash`.
   * Unified asynchronous and synchronous endpoints
   * Less code duplication since we don't have to write special endpoints for every additional communication channel (RESET, MQTT, gRPC)
1. Since infrastructure details are abstracted away behind side-cars, we are very nimble and can adapt  to changes more easily (replacing redis pub/sub with RabbitMq)

### Cons

1. The learning curve is not to be unterestimated
1. Documentation is kept to a minimum

* You are provided with a birds eye view and then pointed to minimal sample implementations. Which is fine if you are into trial and error.
* We spent hours troubleshooting a pub/sub problem just to find that the default in-memory broker does not work as expected. We needed to move to Redis or RabbitMQ for testing.
* A Dappr dashboard is provided, yet it [can't be containerized yet](https://github.com/dapr/dashboard/issues/172)

__For perspective:__ at the time of writing, we are using Dapr v1.9. This framework is only 3 years old. Mind the scope of the problems it wants to solve.

## Tracability

This Demo also includes traceability in the form of [Zipkin].

> Zipkin is a distributed tracing system. It helps gather timing data needed to troubleshoot latency problems in service architectures. Features include both the collection and lookup of this data.

The dashboard is available via

* `http://localhost:9412/zipkin/`

![](./img/zipkin-dependencies_2022-10-30%20at%2013-43-12.png)
![](./img/zipkin-order-trace_2022-10-30%20at%2013-42-40.png)

[Zipkin]: https://zipkin.io/

## Secret Store

Dapr provides APIs for handling secrets over the network

* see [SECRET_STORE.md](/docs/SECRET_STORE.md)

## Security

Depending on the outcome of this [feature request][dapr-https-feature-request], communication between application
- sidecar is unencrypted by default, which means that, for instance, secrets are being transmitted in plain text.

* This is [by design][dapr-https-feature-request-by-design] yet poses the question of who is able to listen to
  a certain network connection. It is to be evaluated if, when hosting applications in cloud environments, network
  traffic is limited to trusted parties only, otherwise traffic is exposed to attack vectors. In doubt, infrastructure
  measures are to be taken to ensure access to this network is restricted (firewall, private network, reverse proxy)
* When referencing secrets in dapr component configuration, secrets are transmitted in plain text between pods / service
  networks. mTLS has to be enabled in this case to ensure dapr-to-dapr communication is private.
* Dapr clients already expose an [undocumented `app-ssl`][dapr-run-flags] sidecar flag that, according to
  [this issue][dapr-https-feature-request-insecure-https] enables insecure HTTPS for sidecars.

[dapr-https-feature-request]: https://github.com/dapr/dapr/issues/3565
[dapr-https-feature-request-by-design]: https://github.com/dapr/dapr/issues/3565#issuecomment-902965895
[dapr-https-feature-request-insecure-https]: https://github.com/dapr/dapr/issues/3565#issuecomment-1438289181
[dapr-run-flags]: https://docs.dapr.io/reference/cli/dapr-run/#flags
