# Secret Store

## Hashicorp Vault

Hashicorp Vault implements a tough security layer that forces its users to
either handle mission critical steps manually or implement additional
steps to ensure that automation is done in a secure way (for instance auto unsealing).

### Demo

![](/img/keyvault-setup.gif)

[sealing]: https://developer.hashicorp.com/vault/docs/concepts/seal
[unsealing]: https://developer.hashicorp.com/vault/docs/concepts/seal
[auto-unseal]: https://developer.hashicorp.com/vault/tutorials/auto-unseal/autounseal-transit

### Sealing

> Very much summarized! \
> See [Sealing] for details!


#### Keys

Secrets are encrypted on disk. Only by unsealing / the application server is able to decrypt server state so that it can perform its functions.
The server is sealed

1. on server boot
1. on exceptions
1. by manually sealing

More precisely, sealing is an additional layer of security over data encryption by making it possible to split up 1 root-key into multiple
parts to ensure that the "key to the castle gates" does not fall into
the wrong hands.

### Unsealing

To [unseal][unsealing] server state, a quorum of seal-keys is required (for instance: 3/5 keys must be present to decrypt the root-key). Given a server requiring only 1 unseal-key, this key is then being used to

1. decrypt the root-key which in turn can
1. decrypts a keyring which then can access keys for
1. decrypting data on disk

This is a _manual process by design_ and has to be done either by UI or CLI. It _can be automated_ by introducing a feature called [auto-unseal]
which utilizes an additional vault server to delegate the unsealing process to a trusted vault environment.

### Setup

1. Setup a vault server instance
    1. docker compose up secret-store-dapr
    1. http://localhost:8200
    1. Create one or many unseal-keys
    1. Unseal and login using the root-key
    1. Create a new key-valut (KV) secret engine (named `secret` by default).
1. Configure [dapr sidecar configuration](/dapr/components/secretstore_vault.yaml)
    1. Adjust `enginePath` to the key-vault name above
   (this name is configurable in the dapr component config)
    1. Update the file referenced by `vaultTokenMountPath` to the
       new root-token
1. Restart secret store

Now you have a fully setup **DEV** key-vault. that can be access

#### Secrets

1. Create a secret called "hello" with any properties you like
1. Access the secret over the web
   ```pwsh
   curl "http://localhost:3500/v1.0/secrets/$daprSecretComponentName/$secretName" | jq
   curl "http://localhost:3500/v1.0/secrets/secret-vault/hello-secret" | jq -c
    # {"hello":"my-lovely-secret-<3"}
   ```


### Troubleshooting

#### Accessing the CLI

To access the vault CLI

1. connect to the docker container
1. provide the root token
1. override the vault adress
   * we have to tell the API to communicate over HTTP instad of HTTPS since we have no
     SSL set up yet

```pwsh
docker exec -ti 'hello-dapr-secret-store' sh
export VAULT_TOKEN='hvs.Zxl4Xv8ANgj0yG9KWbno8A5x'
export VAULT_ADDR='http://localhost:8200'

# unseal vault with 2/3 unseal-keys
vault operator unseal 
# PWsNs84HK2ICkSmpBc0a7iTauEJDYIGF5rg7UT/VYD5b
vault operator unseal
AjQaSuJLdgKhUBrGsEwwGd3upseW4+bCux2RdFMoCAz6

vault kv list secret
vault kv get secret/hello-secret
```

### Security

Since vault is basically a huge bag of secrets to be accessed by tens of services, we have to ensure that none of them leak to the public by transmitting them in cleartext over the network.

![](/img/dapr-vault-http-wireshark-vaN2829STe.gif)

The connection between Dapr and Vault can be secured over TLS. For this we need signed certificates from either an externally managed certificate authority (verisign, lets-encrypt) or sign them ourselves. For this reason, vault brings its own PKI-Engine that we can use as self-hosted root- and intermediate- certificate authority. With it we can generate certificates to be used for network layer security and authorization purposes. Note that security limitations of Dapr still apply (see the security section of this projects README file.)

> For a neat demonstration on how to use the Vault PKI, check out this tutorial (especialy the video demo at the bottom):
> * https://developer.hashicorp.com/vault/tutorials/secrets-management/pki-engine#cleanup

### Experiment: validating TLS encryption between vault and dapr

The goal was to validate TLS encryption between vault and the dapr side car. Realizing how easy it is to sniff secrets freshly off the wire, I thought it valuable / interesting enough to make 2 tiny recordings.

#### Default unencrypted connection

![](/img/dapr-vault-http-wireshark-vaN2829STe.gif)

Of course, data is transmitted in plain text for everyone to see.

#### Enabled TLS encryption between vault and sidecar

![](/img/dapr-vault-https-wiresharlkH12m8as8ox.gif)

Secrets are encrypted between vault and the sidecar, yet expose secrets via HTTP to Dapr users