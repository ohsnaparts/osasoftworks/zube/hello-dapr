#!/bin/bash

# https://asciinema.org/
# https://github.com/marionebl/svg-term-cli

_asciinema_recording="../hello-dapr.asciinema.rec"

cat "$_asciinema_recording" | svg-term --out "$_asciinema_recording.svg" \
	--width 124 \
	--height 24 \
	--padding 4  \
	--window \
	--out "$_asciinema_recording.svg"
