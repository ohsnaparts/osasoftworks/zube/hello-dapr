namespace BattleApi.Models;

public record Trainer(Guid Id, string Name, string Gender, Monster[] Partner);