namespace BattleApi.Models;

public record Battle(Guid Id, Trainer challenger, Trainer opponent);