namespace BattleApi.Models;

public record Monster(Guid Id, string Name, string Gender);