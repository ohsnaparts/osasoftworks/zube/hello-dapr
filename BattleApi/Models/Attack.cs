namespace BattleApi.Models;

public record Attack(Guid Id, string Name);