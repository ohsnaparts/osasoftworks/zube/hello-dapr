using BattleApi.Extensions;

var builder = WebApplication
    .CreateBuilder(args)
    .WithConfiguration();

// Add services to the container.
builder.Services
    .AddRouting()
    .AddOpenApi()
    .AddMessaging()
    .AddConfiguration()
    .AddBlueprints()
    .AddAuthorization()
    .AddDaprClient();

var app = builder.Build();
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection()
    .UseRouting()
    .UseCloudEvents()
    .UseAuthorization()
    .UseOpenApi();
app
    .MapBlueprints()
    .MapDaprEndpoints();

app.Run();