namespace BattleApi.Extensions;

public static class WebApplicationBuilderExtensions
{
    public static WebApplicationBuilder WithConfiguration(this WebApplicationBuilder builder)
    {
        builder.Configuration.AddEnvironmentVariables("CONF_");
        return builder;
    }
}