using BattleApi.Blueprints;
using BattleApi.Configuration;
using BattleApi.Messaging.Publisher;
using Dapr.Client;

namespace BattleApi.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddOpenApi(this IServiceCollection services)
    {
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
        return services;
    }

    public static IServiceCollection AddMessaging(this IServiceCollection services) => services
        .AddTransient<IAttackMonsterMessagePublisher, AttackMonsterV1Publisher>()
        .AddTransient<ICreateBattleMessagePublisher, CreateBattleV1MessagePublisher>()
        .AddTransient<DaprClient>(_ => new DaprClientBuilder().Build());

    public static IServiceCollection AddConfiguration(this IServiceCollection services) => services
        .AddBoundConfiguration<CreateBattleV1PublisherOptions>()
        .AddBoundConfiguration<AttackMonsterV1PublisherOptions>();

    public static IServiceCollection AddBlueprints(this IServiceCollection services) => services
        .AddBlueprint<BattleBlueprint>()
        .AddBlueprint<SubscriptionsBlueprint>();

    private static IServiceCollection AddBoundConfiguration<TOption>(this IServiceCollection services)
        where TOption : class
    {
        services.AddOptions<TOption>().BindConfiguration(typeof(TOption).Name);
        return services;
    }
    
    private static IServiceCollection AddBlueprint<TBlueprint>(this IServiceCollection services) 
        where TBlueprint : class, IBlueprint => services
        .AddTransient<TBlueprint, TBlueprint>();
}