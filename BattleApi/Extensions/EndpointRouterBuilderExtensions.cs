using BattleApi.Blueprints;

namespace BattleApi.Extensions;

public static class EndpointRouterBuilderExtensions
{
    public static IEndpointRouteBuilder MapBlueprints(this IEndpointRouteBuilder routeBuilder) =>
        routeBuilder
            .MapBlueprint<BattleBlueprint>()
            .MapBlueprint<SubscriptionsBlueprint>();

    public static IEndpointRouteBuilder MapDaprEndpoints(this IEndpointRouteBuilder routeBuilder)
    {
        routeBuilder.MapSubscribeHandler();
        return routeBuilder;
    }

    private static IEndpointRouteBuilder MapBlueprint<TBlueprint>(this IEndpointRouteBuilder routeBuilder)
        where TBlueprint : class, IBlueprint
    {
        var blueprint = routeBuilder.ServiceProvider.GetRequiredService<TBlueprint>();
        blueprint.RegisterEndpoints(routeBuilder);
        return routeBuilder;
    }
}