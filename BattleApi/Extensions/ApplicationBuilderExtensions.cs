namespace BattleApi.Extensions;

public static class ApplicationBuilderExtensions
{
    public static IApplicationBuilder UseOpenApi(this IApplicationBuilder builder)
    {
        builder.UseSwagger();
        builder.UseSwaggerUI();
        return builder;
    }
}