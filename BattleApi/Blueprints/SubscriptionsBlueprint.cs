using System.Text.Json;
using BattleApi.Configuration;
using BattleApi.Messaging.Models;
using Microsoft.Extensions.Options;

namespace BattleApi.Blueprints;

// ReSharper disable once ClassNeverInstantiated.Global
public sealed class SubscriptionsBlueprint : IBlueprint
{
    private readonly ILogger<SubscriptionsBlueprint> _logger;

    public SubscriptionsBlueprint(ILogger<SubscriptionsBlueprint> logger)
    {
        this._logger = logger;
    }

    public string Name => nameof(SubscriptionsBlueprint);

    public void RegisterEndpoints(IEndpointRouteBuilder routeBuilder)
    {
        this._logger.LogDebug("Registering blueprint: {Blueprint}", this.Name);
        var publisherOptions = routeBuilder.ServiceProvider
            .GetRequiredService<IOptions<CreateBattleV1PublisherOptions>>();
        
        routeBuilder.MapPost("/subscriptions/create-battle", (object message) =>
        {
            this._logger.LogInformation(
                "Received {MessageType} message: {Message}",
                nameof(CreateBattleV1),
                JsonSerializer.Serialize(message));
            return Results.Ok();
        }).WithTopic(publisherOptions.Value.DaprComponentName, publisherOptions.Value.Topic);


        routeBuilder.MapPost("/subscriptions/attack-monster", (object message) =>
        {
            this._logger.LogInformation(
                "Received {MessageType} message: {Message}",
                nameof(AttackMonsterV1),
                JsonSerializer.Serialize(message));
            return Results.Ok();
        });
    }
}