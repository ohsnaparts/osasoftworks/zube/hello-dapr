namespace BattleApi.Blueprints;

public interface IBlueprint
{
    public string Name { get; }
    public void RegisterEndpoints(IEndpointRouteBuilder routeBuilder);
}