using BattleApi.Messaging.Models;
using BattleApi.Messaging.Publisher;
using BattleApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace BattleApi.Blueprints;

public sealed class BattleBlueprint : IBlueprint
{
    private readonly ILogger<BattleBlueprint> _logger;

    public BattleBlueprint(ILogger<BattleBlueprint> logger)
    {
        this._logger = logger;
    }

    public string Name => nameof(BattleBlueprint);

    public void RegisterEndpoints(IEndpointRouteBuilder routeBuilder)
    {
        this._logger.LogInformation("Registering {Blueprint} endpoints", this.Name);

        routeBuilder.MapPost("/battle/{battleId:guid}/attack", async (
            Guid battleId,
            Guid attackId,
            [FromServices] IAttackMonsterMessagePublisher messagePublisher
        ) =>
        {
            await messagePublisher.PublishAsync(new AttackMonsterV1(battleId, attackId));
            return "";
        });

        routeBuilder.MapPost("/battle/create", async (
            Battle battle,
            [FromServices] ICreateBattleMessagePublisher messagePublisher
        ) =>
        {
            await messagePublisher.PublishAsync(new CreateBattleV1(battle));
            return "";
        });
    }
}