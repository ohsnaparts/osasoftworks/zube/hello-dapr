namespace BattleApi.Configuration;

public abstract class PublisherOptions
{
    public bool Enabled { get; set; } = false;
    public string DaprComponentName { get; set; } = string.Empty;
    public string Topic { get; set; } = string.Empty;
}