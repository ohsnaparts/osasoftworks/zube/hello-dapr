# Battle API

This solution demonstrates the use of the [Dapr Publish & Subscribe][dapr-pub-sub] Building block using ASP .NET.

| Building Block    | Description                                       | Tech Stacks                                                      |
|-------------------|---------------------------------------------------|------------------------------------------------------------------|
| Publish           | Publishes cloudevent topics to a broker           | In-Memory, MQTT, Redis, RabbitMQ, .NET SDK                              |
| Subscribe         | Subscribes and logs published cloudevent messages | In-Memory, MQTT, Redis, RabbitMQ, .ASP NET SDK, Component Configuration |
| Secret Management | Stores service secrets like RabbitMQ credentials  | File                                                             |

## Description

The goal of the Battle API is merely to produce [CloudEvent]s for consumption by other services so we can test Dapr
infrastructure capabilities in a hands-on way.

## Requirements

* docker compose

## Run

```pwsh
docker compose up
```

## Configuration

The application runs out of the box using development values. Service
configuration has been kept in a dedicated

* [/configs](/configs) folder

## Message Brokers

The application supports different message brokers

* In-Memory
* MQTT3
* RabbitMQ

These can be configured dynamically

## Configuration

Configuration is done through appsettings or environment variables.

| Key                                                     | Value                                |
|---------------------------------------------------------|--------------------------------------|
| CONF_AttackMonsterV1PublisherOptions__DaprComponentName | The name of a dapr pub/sub component |
| CONF_CreateBattleV1PublisherOptions__DaprComponentName  | The name of a dapr pub/sub component |

### Subscriptions

This application uses different ways of subscribing to topics.

1. **Programmatically**
   * The subscription endpoint defines a broker/topic it wants to subscribe to.
     To make things simpler, the BattleAPI uses publisher configuration to subscribe (ie.
     no additional configuration is required to see published messages in the log)
1. **Yaml Definition**
   * Some languages don't have the luxury of an SDK at their disposal. The `create-battle-v1`
     topic is therefore configured using a dedicated dapr component definition.

Configuration is optional since default values are provided via application app-settings.

[dapr-pub-sub]: https://docs.dapr.io/developing-applications/building-blocks/pubsub/
[cloudevent]: https://docs.dapr.io/developing-applications/building-blocks/pubsub/pubsub-cloudevents/