using BattleApi.Configuration;
using BattleApi.Messaging.Models;
using Dapr.Client;
using Microsoft.Extensions.Options;

namespace BattleApi.Messaging.Publisher;

public sealed class CreateBattleV1MessagePublisher : AbstractDaprMessagePublisher<CreateBattleV1>,
    ICreateBattleMessagePublisher
{
    private readonly IOptions<CreateBattleV1PublisherOptions> _options;

    public CreateBattleV1MessagePublisher(
        ILogger<CreateBattleV1MessagePublisher> logger,
        IOptions<CreateBattleV1PublisherOptions> options,
        DaprClient daprClient
    ) : base(logger, daprClient)
    {
        this._options = options;
    }

    protected override PublisherOptions PublisherOptions => this._options.Value;
}