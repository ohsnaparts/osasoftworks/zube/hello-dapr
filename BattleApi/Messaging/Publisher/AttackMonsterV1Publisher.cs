using BattleApi.Configuration;
using BattleApi.Messaging.Models;
using Dapr.Client;
using Microsoft.Extensions.Options;

namespace BattleApi.Messaging.Publisher;

public sealed class AttackMonsterV1Publisher : AbstractDaprMessagePublisher<AttackMonsterV1>,
    IAttackMonsterMessagePublisher
{
    private readonly IOptions<AttackMonsterV1PublisherOptions> _options;

    public AttackMonsterV1Publisher(
        ILogger<AttackMonsterV1Publisher> logger,
        IOptions<AttackMonsterV1PublisherOptions> options,
        DaprClient daprClient
    ) : base(logger, daprClient)
    {
        this._options = options;
    }

    protected override PublisherOptions PublisherOptions => this._options.Value;
}