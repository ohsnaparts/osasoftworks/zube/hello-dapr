using BattleApi.Configuration;
using Dapr.Client;

namespace BattleApi.Messaging.Publisher;

public abstract class AbstractDaprMessagePublisher<TMessage> : IMessagePublisher<TMessage>
{
    private readonly ILogger _logger;
    private readonly DaprClient _daprClient;

    protected AbstractDaprMessagePublisher(ILogger logger, DaprClient daprClient)
    {
        this._logger = logger;
        this._daprClient = daprClient;
    }

    protected abstract PublisherOptions PublisherOptions { get; }

    public Task PublishAsync(TMessage message, CancellationToken cancellationToken = default)
    {
        this._logger.LogDebug(
            "Publishing message to {PubSubName}:{Topic}",
            this.PublisherOptions.DaprComponentName,
            this.PublisherOptions.Topic);

        return this._daprClient.PublishEventAsync(
            this.PublisherOptions.DaprComponentName,
            this.PublisherOptions.Topic,
            message,
            new Dictionary<string, string>
            {
                {"rawPayload", "false"}
            },
            cancellationToken);
    }
}