namespace BattleApi.Messaging.Publisher;

public interface IMessagePublisher<TMessage>
{
    Task PublishAsync(TMessage message, CancellationToken cancellationToken = default);
}