using BattleApi.Messaging.Models;

namespace BattleApi.Messaging.Publisher;

public interface ICreateBattleMessagePublisher : IMessagePublisher<CreateBattleV1>
{
}