using BattleApi.Messaging.Models;

namespace BattleApi.Messaging.Publisher;

public interface IAttackMonsterMessagePublisher : IMessagePublisher<AttackMonsterV1>
{
}