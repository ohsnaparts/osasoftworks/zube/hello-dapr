namespace BattleApi.Messaging.Models;

public record AttackMonsterV1(
    Guid BattleId,
    Guid AttackId);