using BattleApi.Models;

namespace BattleApi.Messaging.Models;

public record CreateBattleV1(Battle Battle);