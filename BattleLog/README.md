# Battle Log

This service acts as a demo project for Dapr buldings blocks.

| Building Block    | Description                                          | Tech Stacks               |
|-------------------|------------------------------------------------------|---------------------------|
| Pub/Sub           | Subscribes to battle events                          | to be implemented => [#4] |
| State Management  | Keeps track of battle progress in a persistent store | In-Memory, Redis, MongoDB |
| Secret Management | Stores service secrets like mongodb root credentials | In-Memory, File           |

[Deployment Diagram](/docs/battle-log-deployment.puml)
[#4]:

## Requirements

* python3.9+
* docker compose
* powershell

## Run

```pwsh
# unless you made changes to the code:
docker compose up
# otherwise
docker compose up --build

cd scripts
./New-BattleLog.ps1
./New-BattleLog.ps1
./Get-BattleLog.ps1
# attack_count date
# ------------ ----
#            4 2023-05-19

# To test pub/sub
./Publish-Attack.ps1
# The server log should show a message receipt log entry
#>
```

## Configuration

The application should run out of the box using development values. Service
configuration has been kept in a dedicated

* [/configs](/configs) folder

## State Stores

The application supports different state stores

* In-Memory
* Redis
* MongoDB

These can be configured dynamically

## Configuration

Configuration is done exclusively through environment variables.

Since documentation tends to run out of date, please check out the settings file for the keys available

* [./settings/app_settings.py](./settings/app_settings.py)

Configuration is optional since default values are provided when bootstrapping the application
