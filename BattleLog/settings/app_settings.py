from config import Configuration, ConfigurationSet, config_from_env


class AppSettings:
    _configuration: Configuration

    def __init__(self, configuration: Configuration):
        self._configuration = configuration

    @property
    def dapr_state_store_name(self):
        return self._configuration.get_str("DAPR_STATE_STORE_NAME")
    
    # Subscripber options for the attack-monster-v1 topic
    @property
    def attack_monster_v1_subscriber__dapr_component_name(self):
        return self._configuration.get_str("AttackMonsterV1Subscriber_DaprComponentName")
    @property
    def attack_monster_v1_subscriber__topic(self):
        return self._configuration.get_str("AttackMonsterV1Subscriber_Topic")
    
    # Subscripber options for the create-battle-v1 topic
    @property
    def create_battle_v1_subscriber__dapr_component_name(self):
        return self._configuration.get_str("CreateBattleV1Subscriber_DaprComponentName")
    @property
    def create_battle_v1_subscriber__topic(self):
        return self._configuration.get_str("CreateBattleV1Subscriber_Topic")
    
    
    # Mind the prefix down below. Given a prefix CONF and a separator __, 
    # it means that every environment variable has to begin with "CONF__"
    @staticmethod
    def load():
        return AppSettings(ConfigurationSet(
            config_from_env(prefix='CONF', separator='__')
        ))
