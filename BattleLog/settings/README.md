# Settings

This is a folder holding python modules used for configuring application logic.
Unfortunately naming clashes are unavoidable in this case. 

For infrastructure configuration, see the [/configs](/configs) folder.
