Param(
    [Parameter()]
    [string] $DarpBaseUri = 'http://localhost:3500'
)

Function Publish-Attack {
    Param(
        [Parameter(Mandatory)]
        [string] $BaseUri,
        # --------------------
        [Parameter()]
        [string] $Topic = 'attack-monster-v1',
        # --------------------
        [Parameter()]
        [string] $DaprPubSubName = 'pubsub-in-memory'
    )

    $Uri = "$BaseUri/v1.0/publish/$DaprPubSubName/$Topic"
    @{
        id = Get-Random -Minimum 0 -Maximum 9999
        specversion = 1.0
        type = "attack-monster-v1"
        source = $MyInvocation.MyCommand.Name
        data = 'test'
    } | ConvertTo-Json | Invoke-RestMethod `
        -Method Post `
        -Uri $Uri `
        -Headers @{
            'Content-Type' = 'application/cloudevents+json' 
        }
}

Publish-Attack -BaseUri $DarpBaseUri