Param(
    [Parameter()]
    [string] $BaseUri = 'http://localhost:5010'
)

Function New-BattleLog {
    Param(
        [Parameter(Mandatory)]
        [string] $BaseUri
    )

    Invoke-RestMethod -Method Post -Uri "$BaseUri/battle-log"
}

New-BattleLog -BaseUri $BaseUri
