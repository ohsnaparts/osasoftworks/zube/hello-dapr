Param(
    [Parameter()]
    [string] $BaseUri = 'http://localhost:5010'
)

Function Get-BattleLog {
    Param(
        [Parameter(Mandatory)]
        [string] $BaseUri
    )

    Invoke-RestMethod -Method Get -Uri "$BaseUri/battle-log"
}

Get-BattleLog -BaseUri $BaseUri