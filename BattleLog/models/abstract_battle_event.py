import datetime
from marshmallow import Schema, fields
from utils.serializable import IJsonSerializable


class AbstractBattleEvent(IJsonSerializable):
    date: datetime.date
    event_type: str
    version: int

    def __init__(self, date: datetime.date, event_type: str, version: int):
        self.date = date
        self.event_type = event_type
        self.version = version

    def __repr__(self):
        return self.to_json()


class AbstractBattleEventSchema(Schema):
    date = fields.Date()
    event_type = fields.String()
    version = fields.Integer()
