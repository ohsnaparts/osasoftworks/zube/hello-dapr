import datetime
from typing import Any, Dict
from marshmallow import fields, post_load

from models.abstract_battle_event import AbstractBattleEventSchema, AbstractBattleEvent


class AttackMonsterEvent(AbstractBattleEvent):
    damage: int

    @staticmethod
    def get_event_type():
        return 'attack-monster-v1'

    def __init__(self, date: datetime.date, damage: int, version: int, event_type: str):
        super().__init__(date, event_type, version)
        self.date = date
        self.damage = damage

    def to_json(self) -> str:
        return AttackMonsterEventSchema().dumps(self)

    @staticmethod
    def from_json(json: Dict[str, Any]):
        return AttackMonsterEventSchema().load(json)


class AttackMonsterEventSchema(AbstractBattleEventSchema):
    damage = fields.Integer()

    @post_load
    def _to_object(self, data, **kwargs) -> AttackMonsterEvent:
        return AttackMonsterEvent(**data)
