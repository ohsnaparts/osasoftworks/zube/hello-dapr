import datetime
from http import HTTPStatus

from flask import Blueprint

from models.attack_monster_event import AttackMonsterEvent
from repository.attack_monster_event_read_repository import AttackMonsterEventReadRepository
from repository.attack_monster_event_write_repository import AttackMonsterWriteRepository
from models.abstract_battle_event import AbstractBattleEvent


class BattleLogBlueprint(Blueprint):
    _name: str = 'battle-log'
    _url_prefix: str = '/battle-log'
    _read_repository: AttackMonsterEventReadRepository
    _write_repository: AttackMonsterWriteRepository

    def __init__(self, read_repository: AttackMonsterEventReadRepository, write_repository: AttackMonsterWriteRepository):
        super().__init__(self._name, __name__, url_prefix=self._url_prefix)
        self.read_repository = read_repository
        self.write_repository = write_repository
        self._register_endpoints()

    def _register_endpoints(self):
        @self.route('', methods=['GET'])
        def get_log():
            print(f'GET {self._url_prefix} called', flush=True)
            log = self.read_repository.get_event()
            if not log:
                return "", HTTPStatus.NOT_FOUND
            return log.to_json(), HTTPStatus.OK

        @self.route('', methods=['POST'])
        def post_log():
            print(f'POST {self._url_prefix} called', flush=True)
            log = self.read_repository.get_event() or AttackMonsterEvent(datetime.date.today(),
                                                                         10,
                                                                         1,
                                                                         AttackMonsterEvent.get_event_type())
            log.damage += 1
            self.write_repository.set_log(log)
            return "", HTTPStatus.OK
