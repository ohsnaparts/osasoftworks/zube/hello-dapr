from flask import Blueprint, request
from models.abstract_battle_event import AbstractBattleEvent
from settings.app_settings import AppSettings



class DaprSubscribeBlueprint(Blueprint):
    _name: str = 'subscribe'
    _url_prefix: str = '/dapr/subscribe'
    _settings: AppSettings

    def __init__(self, settings: AppSettings):
        super().__init__(self._name, __name__, url_prefix=self._url_prefix)
        self._settings = settings
        self._register_endpoints()

    def _register_endpoints(self):
        @self.route('', methods=['GET'])
        def get_subscriptions():
            return [
                dict(
                    pubsubname=self._settings.attack_monster_v1_subscriber__dapr_component_name,
                    topic=self._settings.attack_monster_v1_subscriber__topic,
                    routes=dict(
                        rules=[
                            dict(
                                # https://github.com/google/cel-spec/blob/master/doc/intro.md
                                match='size(event.data) > 0',
                                path='/dapr/subscribe/message'
                            ),
                        ]
                    )
                ),
                dict(
                    pubsubname=self._settings.create_battle_v1_subscriber__dapr_component_name,
                    topic=self._settings.create_battle_v1_subscriber__topic,
                    routes=dict(
                        rules=[
                            dict(
                                # https://github.com/google/cel-spec/blob/master/doc/intro.md
                                match='size(event.data) > 0',
                                path='/dapr/subscribe/message'
                            ),
                        ]
                    )
                )
            ], 200

        @self.route(f'/message', methods=['POST'])
        def _receive_message():
            message = request.data
            print(f"Received message: {message}", flush=True)
            return message, 200
