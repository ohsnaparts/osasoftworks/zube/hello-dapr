#!/bin/bash

pip3 install -r requirements.txt

# flask depends on this env variable to find the main file
export FLASK_APP="app.py"
export FLASK_DEBUG=${FLASK_DEBUG:=1}
export FLASK_PORT=${FLASK_PORT:=5010}
export DAPR_HTTP_PORT=${DAPR_HTTP_PORT:=3500}
export ENVIRONMENT="${ENVIRONMENT:=dev}"

# base app configuration
export CONF__DAPR_STATE_STORE_NAME="${CONF__DAPR_STATE_STORE_NAME:=my-inmemory-store}"
export CONF__AttackMonsterV1Subscriber_DaprComponentName="${CONF__AttackMonsterV1Subscriber_DaprComponentName:=pubsub-in-memory}"
export CONF__AttackMonsterV1Subscriber_Topic="${CONF__AttackMonsterV1Subscriber_Topic:=attack-monster-v1}"
export CONF__CreateBattleV1Subscriber_DaprComponentName="${CONF__CreateBattleV1Subscriber_DaprComponentName:=pubsub-in-memory}"
export CONF__CreateBattleV1Subscriber_Topic="${CONF__CreateBattleV1Subscriber_Topic:=create-battle-v1}"

if [ $ENVIRONMENT == "local" ]; then
  dapr run --app-id battle-log \
    --app-port $FLASK_PORT \
    --dapr-http-port $DAPR_HTTP_PORT \
    --config './configs/local/dapr/config.yaml' \
    --components-path './configs/local/dapr/components' \
    --enable-app-health-check \
    --app-health-check-path "/healthz" \
    --app-health-probe-interval 1 \
    --app-health-probe-timeout 500 \
    --app-health-threshold 3 \
    -- \
    flask run --host=0.0.0.0 --port $FLASK_PORT
else
  flask run --host=0.0.0.0 --port $FLASK_PORT
fi
