# noinspection PyProtectedMember
from dapr.clients.grpc._response import DaprResponse
from dapr.clients import DaprClient
from models.attack_monster_event import AttackMonsterEvent
from settings.app_settings import AppSettings
from utils.serializable import IJsonSerializable


class AttackMonsterWriteRepository:
    _configuration: AppSettings
    _document_key = "battle_log"

    def __init__(self, configuration: AppSettings):
        self._configuration = configuration

    def set_log(self, event: AttackMonsterEvent) -> None:
        self._save_state(event)

    def _save_state(self, obj: IJsonSerializable) -> DaprResponse:
        value = obj.to_json()
        store = self._configuration.dapr_state_store_name

        print(f"Persisting value {value} to {store}", flush=True)
        with DaprClient() as dapr:
            return dapr.save_state(
                store_name=store,
                key=self._document_key,
                value=value
            )
