# noinspection PyProtectedMember
from dapr.clients.grpc._response import StateResponse
from typing import Dict, Optional
from dapr.clients import DaprClient
from models.attack_monster_event import AttackMonsterEvent
from settings.app_settings import AppSettings


class AttackMonsterEventReadRepository:
    _configuration: AppSettings
    _document_key = "battle_log"

    def __init__(self, configuration: AppSettings):
        self._configuration = configuration

    def get_event(self) -> AttackMonsterEvent:
        json = self._get_json_state(self._document_key)
        return AttackMonsterEvent.from_json(json) if json else None

    def _get_json_state(self, key: str) -> Optional[Dict[str, object]]:
        min_json_char_count = len("\"{}\"")
        result = self._get_state(key)
        value_found = len(result.text()) >= min_json_char_count
        return result.json() if value_found else None

    def _get_state(self, key: str) -> StateResponse:
        store_name = self._configuration.dapr_state_store_name
        print(f'Retrieving state for {key} from {store_name}', flush=True)
        with DaprClient() as dapr:
            return dapr.get_state(store_name=store_name, key=key)
