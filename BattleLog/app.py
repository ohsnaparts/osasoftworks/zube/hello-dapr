from flask import Flask
from repository.attack_monster_event_read_repository import AttackMonsterEventReadRepository
from repository.attack_monster_event_write_repository import AttackMonsterWriteRepository
from blueprints.subscriptions_blueprint import DaprSubscribeBlueprint
from blueprints.battle_log_blueprint import BattleLogBlueprint
from settings.app_settings import AppSettings
from http import HTTPStatus

app_settings = AppSettings.load()
write_repository = AttackMonsterWriteRepository(app_settings)
read_repository = AttackMonsterEventReadRepository(app_settings)

app = Flask(__name__)
app.register_blueprint(DaprSubscribeBlueprint(app_settings))
app.register_blueprint(BattleLogBlueprint(
    read_repository,
    write_repository
))


@app.route('/')
def hello_world():  # put application's code here
    return '/battle-log [POST/GET]', HTTPStatus.OK


@app.route('/healthz')
def health():
    return '', HTTPStatus.OK


if __name__ == '__main__':
    app.run()
